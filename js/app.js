var countdownCallback = function(countdownTime) {
    // Get the current time and round down to the nearest second
    var currentTime = new Date();

    currentTime.setMilliseconds(0);

    // Get the difference between our target time and right now in seconds
    var timeDiff = (countdownTime.getTime() - currentTime.getTime()) / 1000;

    // Once we find out if we're pos or neg, convert timeDiff to a positive number
    var plusOrMinus = timeDiff < 0 ? '-' : '';

    timeDiff = Math.abs(timeDiff);

    // Pull the hours, minutes, and seconds from the time difference and pad with 0s, maybe
    var hours = Math.floor(timeDiff / 3600);
    var minutes = Math.floor(timeDiff / 60) % 60;
    var seconds = timeDiff % 60;

    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    // Nice, human-readable string
    var countdownString = plusOrMinus + hours + ':' + minutes + ':' + seconds;

    $('#countdown').html(countdownString);
};

var countdownFactory = function(timeValue) {
    // Convert the string to a Date Object and round down it to the nearest minute
    var countdownTime = new Date();
    var parsedTime = timeValue.match(/(\d+):(\d+)(p?)/);

    countdownTime.setHours(parseInt(parsedTime[1]) + (parsedTime[3] ? 12 : 0));
    countdownTime.setMinutes(parseInt(parsedTime[2]));
    countdownTime.setSeconds(0);
    countdownTime.setMilliseconds(0);

    // Start time is right now, and we round down to the nearest second
    var startTime = new Date();

    startTime.setMilliseconds(0);

    // If the selected countdown time is in the "past" we convert it to the next day
    if (countdownTime.getTime() < startTime.getTime()) {
        countdownTime.setDate(countdownTime.getDate() + 1);
    }

    // Return our callback method initialized with the appropriate variables
    return function() {
        countdownCallback(countdownTime);
    };
};

var timeSelectSubmit = function(event) {
    event.preventDefault();

    // Check to make sure a value was selected
    var timeValue = $('#time-select').val();

    if (!timeValue) {
        $('#time-select-alert').removeClass('hidden')
            .addClass('shown');

        return;
    }

    // Get the callback method setInterval will execute once per second
    var updateCountdown = countdownFactory(timeValue);

    updateCountdown();
    setInterval(updateCountdown, 1000);

    // Hide the initial time select window, display countdown
    $('#time-select-window').removeClass('shown')
        .addClass('hidden');

    $('#countdown-window').removeClass('hidden')
        .addClass('shown');
};

$(document).ready(function() {
    $('#time-select').timepicker({ 
        'scrollDefault' : 'now',
        'step' : 15
    });

    $('#time-select-submit').on('click', timeSelectSubmit);
});
